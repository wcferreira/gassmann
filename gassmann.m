function [Vp_new,Vs_new, rho_sat] = gassmann(T,P,S,GOR,G,rho_o,WS,Vp,Vs,rho,Vsh,phi,WS_new,itype)

P=P*6.894757*0.001;
S = S/1000000;
Vp = Vp*0.000305;
Vs = Vs*0.000305;
rho_o = 141.5/(rho_o+131.5);


%Step 1 - Evaluate bulkd modulus Kmatrix and density of matrix
[K_matrix, rho_matrix] = Kmatrix(Vsh);

%Step 2 - Evaluate bulk mudulus and density of brine
[K_brine,rho_brine] = Kbrine(T,P,S);


%Step 3 - Evaluate bulk mudulus of hydrocarbon (choose one)
[K_hyc, rho_hyc] = Koil(T,P,GOR,G,rho_o);
%[K_hyc,rho_hyc] = Kgas(T,P,G);

%Step 4 - Evaluate bulkd modulus of fluid

[K_fl,rho_fl] = Kfl(WS,K_brine,rho_brine,K_hyc,rho_hyc);

%Step 5 - Evaluate the initial Ksat and shear mudulus
K_sati = Ksati(rho,Vp,Vs);
shear = Shear_Modulus(rho,Vs);

%Step 6 - Evaluate Kframe
K_frame = Kframe(K_sati,K_matrix,K_fl,phi);


%After this point we are going to estimate the parameters for the new fluid

T = T;  %Temperature
P = P;
S = S;




%Step 7 - Evaluate bulk mudulus of desired hydrocarbon (choose one)
switch itype
    case  1
        [KN_hyc,rhoN_hyc] = Koil(T,P,GOR,G,rho_o);
    case  2
        [KN_hyc,rhoN_hyc] = Kgas(T,P,G);
    case  3
        [KN_hyc,rhoN_hyc] = Kbrine(T,P,S);
end


%Step 8 - Evaluate bulkd modulus of fluid
[K_fl_new,rho_fl_new] = Kfl(WS_new,K_brine,rho_brine,KN_hyc,rhoN_hyc);

%Step 9 - Evaluate bulk mudulus of saturated rock after fluid substitution
K_sat = Ksat(K_frame,K_matrix,K_fl_new,phi);
rho_sat = rhosat(rho_fl_new,rho_matrix,phi);

%Step 10 - Evaluate seismic velocities (P-S waves)
Vp_new = sqrt((K_sat + (4/3)*shear)/rho_sat);
Vs_new = sqrt(shear/rho_sat);

Vp = Vp*3280.84;
Vs = Vs*3280.84;
Vp_new = Vp_new*3280.84;
Vs_new = Vs_new*3280.84;


