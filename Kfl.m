function [K_fl,rho_fl] = Kfl(WS,K_brine,rho_brine,K_hyc,rho_hyc)


%Mixed pore fluid phase through Wood's equation
%WS = water saturation

K_fl = 1/((WS/K_brine) + ((1-WS)/K_hyc));
rho_fl = WS*rho_brine + (1-WS)*rho_hyc;

end