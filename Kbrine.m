function [Kbr,rho_brine] = Kbrine(T,P,S)

%The function was wrote based on Geohorizons (2006),Batze and Wang (1992)

%Brine density estimation (rho_brine)

%S = salinity
%P(MPa) in-situ pressure
%T (Celsius) in-situ temperature
%rho_w = density of water

rho_w = 1 + (10^(-6))*(-80*T - 3.3*T^2 + 0.00175*T^(3) + ...
    489*P - 2*T*P + 0.016*P*T^(2) - (1.3*10^(-5))*P*T^(3) - ...
    0.333*P*P - 0.002*T*P^(2));

rho_brine = rho_w + 0.668*S + 0.44*S.^2 + (S*10^(-6))*(300*P - ...
    2400*P*S + T*(80 + 3*T - 3300*S - 13*P + 47*P*S));

    
%Brine velocity estimation (V_brine)
%the units of V_brine is (m/s)

%the following coefficients will be used to compute water velocity (Batzle
%and Wang, 1992)
W = zeros(5,4);

W(1,1) = 1402.85;           W(1,2) = 1.524;             W(1,3) = 3.437*10^(-3);     W(1,4) = -1.197*10^(-5);      
W(2,1) = 4.871;             W(2,2) = -0.0111;           W(2,3) = 1.739*10^(-4);     W(2,4) = -1.628*10^(-6);
W(3,1) = -0.04783;          W(3,2) = 2.747*10^(-4);     W(3,3) = -2.135*10^(-6);    W(3,4) = 1.237*10^(-8);
W(4,1) = 1.487*10^(-4);     W(4,2) = -6.503*10^(-7);    W(4,3) = -1.455*10^(-8);    W(4,4) = 1.327*10^(-10);
W(5,1) = -2.197*10^(-7);    W(5,2) = 7.987*10^(-10);    W(5,3) = 5.230*10^(-11);    W(5,4) = -4.614*10^(-13);


sum=0;
for i=1:5 for j=1:4
        
sum = sum+W(i,j)*T^(i-1)*P^(j-1); end
end

V_w = sum;


    
    
V_brine1 = V_w + S*(1170 - 9.6*T + 0.055*T^(2) - (8.5*10^(-5))*T^(3));

V_brine2 = V_brine1 +  S*(2.6*P - 0.0029*T*P - 0.0476*P^(2));
V_brine = V_brine2 + (S^(1.5))*(780 -10*P + 0.16*P^(2)) - 1820*S^(2);

%Computation of brine bulk modulus
%The unit is GPA (Gigapascal)

Kbr = rho_brine*(V_brine^(2))*10^(-6);



