function K = Ksati(rho,Vp,Vs)

Vp2 = Vp.^2;
Vs2 = Vs.^2;

K = rho.*(Vp2 - (4/3).*Vs2);
end