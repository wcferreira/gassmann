function Sws = Sw(Vsh, phi_e,RESD)

%phi_e = effective porosity (corrected for shale content)
%Vsh = Shale Volume
%a = tortuosity
%RWT = water resistivity at formation temperature
%m = cementation expoent
%RSH = shale resistivity
%RESD = deep resistivity log reading
%n = saturation expoent

a = 0.62; %recommended parameters for sandstone
m = 2.15; %// sandstone
n = 2;
RSH = 4; %this value can range from 1 to 10, the default will be 4;
RWT = 0.06; %we need an equation to calculate this.

tt = size(phi,1);
Sws = NaN(tt,1);

for i=1:tt

    if phi_e(i,1) > 0
        C = (1 - Vsh(i,1))*a*RWT/(phi_e(i,1)^m);
        D = C*Vsh(i,1)/(2*RSH);
        E = C/RESD(i,1);

        Sws(i,1) = ((D^2 + E)^(0.5) - D)^(2/n);

    else
        Sws(i,1) = 1;
    end
end