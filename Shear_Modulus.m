function u = Shear_Modulus(rho,Vs)


Vs2 = Vs.^2;
u = rho.*Vs2;
end