function K_frame = Kframe(K_sat,K_matrix,K_fl,phi)


factor1 = phi*K_matrix/K_fl;
factor2 = K_sat/K_matrix;


K_frame = (K_sat*(factor1 + 1 - phi) - K_matrix)/(factor1 + factor2 - 1 - phi);


end


