function rho_sat= rhosat(rho_fl,rho_matrix,phi)

rho_sat = phi*rho_fl + (1 - phi)*rho_matrix;


end
