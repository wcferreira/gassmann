function [Kg,rho_gas] = Kgas(T,P,G)



%Computing the pseudo-reduced parameters T_pr and P_pr

T_pr = (T + 273.15)/(94.72 + 170.75*G);
P_pr = (P)/(4.892 - 0.4048*G);


%Compressibility factor estimation Z
%T_pr = pseudo-reduced temperature
%P_pr = pseudo-reduced pressure

factor1 = 1/T_pr;
factor2 = (P_pr^(1.2))/T_pr;

E1 = 0.109*((3.85 - T_pr)^(2));
E = E1*exp(-(0.45 + 8*(0.56 - factor1)^(2))*factor2);

Z1 = (0.03 + 0.00527*(3.5 - T_pr)^(3))*P_pr;
Z = Z1 + (0.642*T_pr - 0.007*T_pr^(4) - 0.52) + E;


%Computing density of gas
%R = gas constant
%G (gravity of gas) is in API
%Z = compressibility factor

R = 8.314;
rho_gas = (28.8*G*P)/(Z*R*(T+273.15));


%computing yo
yo1 = 0.85 + (5.6/(P_pr +2)) + (27.1/((P_pr + 3.5)^(2))); 
yo = yo1 - 8.7*exp(-0.65*(P_pr+1));


%computing factor F
factor3 = (P_pr^(0.2))/T_pr;

F1 = -1.2*factor3*(0.45 + 8*(0.56 - factor1)^(2));
F = F1*exp(-(0.45 + 8*(0.56 - factor1)^(2))*factor2);

%computing dZdP_pr

dZdP_pr = 0.03 + 0.00527*(3.5 - T_pr)^(3) + 0.109*((3.85 - T_pr)^(2))*F;


%computing the gas bulk modulus K_gas
factor4 = P_pr/Z;
factor5 = dZdP_pr;

Kg = (P/(1 - factor4*factor5))*yo/1000;



