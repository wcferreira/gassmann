function [K_matrix,rho_matrix] = Kmatrix(Vsh)

%Bulk modulus for clay and quartz can be found in textbooks


Kclay = 25;  %(GPa = Gigapascal)
Kqtz  = 40; %(GPa)
rho_clay = 2.55; %(g/cc)
rho_qtz  = 2.65; %g/cc)


Vclay = (70/100).*Vsh;
Vqtz = 1 - Vclay;


K_Voigt = (Vclay.*Kclay + Vqtz.*Kqtz);
K_Reuss = 1/((Vclay./Kclay) + (Vqtz./Kqtz));

K_matrix = (1/2).*(K_Voigt + K_Reuss);

rho_matrix = Vclay.*rho_clay + Vqtz.*rho_qtz;

end