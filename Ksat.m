function K_sat = Ksat(K_frame,K_matrix,K_fl,phi)

factor1 = (K_frame/K_matrix);
factor2 = (phi/K_fl);
factor3 = (1-phi)/K_matrix;
factor4 = (K_frame/(K_matrix^2));


K_sat = K_frame + (((1 - factor1)^(2))/(factor2 + factor3 - factor4));

end