%Parameters input for the initial stage
%itype control the kind of fluid substitution desired
            %itype = 1 for Oil
            %itype = 2 for Gas
            %itype = 3 for Brine

clear all
clc
T = 150.00;  %Temperature
P = 3200.00;
S = 3800;
WS = 1.00;  %water saturation

Vp = 8200.00;
Vs = 3833.00;
rho= 2.30;
Vsh = 0;


GOR = 160.0;
rho_o = 42;
G=0.9;
%porosity
phi = 0.4;

WS_new = 0;
itype = 2;

[Vp_new,Vs_new, rho_sat] = gassmann(T,P,S,GOR,G,rho_o,WS,Vp,Vs,rho,Vsh,phi,WS_new,itype);
Vp_new,Vs_new,rho_sat