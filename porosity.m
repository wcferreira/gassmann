function phi_e = porosity(Vsh,rho_measured)


%Vsh = Shale Volume
%rho_sh = shale density (g/cc)
%rho_ma = matrix density (g/cc)
%rho_fl = fluid density (g/cc) (usually the equipment penetrate only in the
%invaded zone so rho_fl = rho_mud_filtrate

rho_ma = 2.65; %sandstone
rho_fl = 1; %water 
rho_sh =2.60; %average value for shale(?)

phi_e = (rho_ma.*(1-Vsh) + Vsh.*rho_sh - rho_measured)./(rho_ma - rho_fl);


end