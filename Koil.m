function [K_oil,rho_oil] = Koil(T,P,GOR,G,rho_o)

%GOR = gas-to-oil-ratio
%G = Gas gravity(API)

%computing oil density
%rho_s = saturation density
%rho_o is the reference density of oil measured at 15.6 celsius
%Bo is the formation volume factor
Rg = GOR;
factor1 = sqrt(G/rho_o);

Bo = 0.972 + 0.00038*(2.495*Rg*factor1 + T + 17.8)^(1.175);

rho_s = (rho_o + 0.0012*Rg*G)/Bo;

rho1 = rho_s + (0.00277*P - ((1.71*0.0000001))*P^(3))*(rho_s - 1.15)^(2) + (3.49*10^(-4))*P;
rho_oil = rho1/(0.972 + (3.81*10^(-4))*(T+17.78)^(1.175));


%computing the Velocity of oil
%rho_ps is the pseudo density
rho_ps = rho_o/((1+0.001*Rg)*Bo);

factor2 = sqrt(rho_ps/(2.6 - rho_ps));
factor3 = sqrt((18.33/rho_ps) - 16.97);

V_oil = 2096*factor2 - 3.7*T + 4.64*P + 0.0115*(factor3 - 1)*T*P;

%computing the bulk modulus of the oil
%the units of K-oil is GPa (Gigapascal)
K_oil = rho_oil*(V_oil^(2))*10^(-6);